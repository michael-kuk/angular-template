/* jshint node:true */

var
  gulp = require('gulp'),
  usemin = require('gulp-usemin'),
  wrap = require('gulp-wrap'),
  connect = require('gulp-connect'),
  watch = require('gulp-watch'),
  minifyCss = require('gulp-cssnano'),
  minifyJs = require('gulp-uglify'),
  concat = require('gulp-concat'),
  less = require('gulp-less'),
  sass = require('gulp-sass'),
  gutil = require('gulp-util'),
  rename = require('gulp-rename'),
  plumber = require('gulp-plumber'),
  sourcemaps = require('gulp-sourcemaps'),
  minifyHTML = require('gulp-htmlmin');

var paths = {
  scripts: ['assets/js/**/*.module.js', 'assets/js/**/*.js'],
  styles: 'assets/scss/app.scss',
  images: 'assets/img/**/*.*',
  templates: 'assets/templates/**/*.html',
  index: 'assets/index.html',
  bower_fonts: 'assets/lib/**/*.{ttf,woff,eof,svg,woff2}',
  out: 'public'
};

function swallowError(error) {
  console.log(error.toString());
  this.emit('end');
}

/**
 * Handle bower components from index
 */
gulp.task('usemin', function() {
  return gulp.src(paths.index)
    .pipe(usemin({
      js: [minifyJs(), 'concat'],
      css: [minifyCss({
        keepSpecialComments: 0
      }), 'concat'],
    }))
    .pipe(gulp.dest(paths.out + '/'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-bower_fonts']);

gulp.task('copy-bower_fonts', function() {
  return gulp.src(paths.bower_fonts)
    .pipe(rename({
      dirname: '/fonts'
    }))
    .pipe(gulp.dest(paths.out));
});

/**
 * Handle custom files
 */
gulp.task('build-custom', ['custom-images', 'custom-js', 'custom-sass', 'custom-templates']);

gulp.task('custom-images', function() {
  return gulp.src(paths.images)
    .pipe(gulp.dest(paths.out + '/img'));
});

gulp.task('custom-js', function() {
  return gulp.src(paths.scripts)
    .pipe(concat('app.min.js'))
    .pipe(sourcemaps.init())
    .pipe(minifyJs())
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest(paths.out + '/'));
});

gulp.task('custom-less', function() {
  return gulp.src(paths.styles)
    .pipe(less())
    .pipe(gulp.dest(paths.out + '/css'));
});

gulp.task('custom-sass', function() {
  return gulp.src(paths.styles)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', swallowError)
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest(paths.out));
});

gulp.task('custom-templates', function() {
  return gulp.src(paths.templates)
    .pipe(minifyHTML())
    .pipe(gulp.dest(paths.out + '/templates'));
});

/**
 * Watch custom files
 */
gulp.task('watch', function() {
  gulp.watch([paths.images], ['custom-images']);
  gulp.watch([paths.styles], ['custom-sass']);
  gulp.watch([paths.scripts], ['custom-js']);
  gulp.watch([paths.templates], ['custom-templates']);
  gulp.watch([paths.index], ['usemin']);
});

/**
 * Live reload server
 */
gulp.task('webserver', function() {
  connect.server({
    root: paths.out + '/',
    livereload: true,
    port: 8888
  });
});

gulp.task('livereload', function() {
  gulp.src([paths.out + '/**/*.*'])
    .pipe(watch([paths.out + '/**/*.*']))
    .pipe(connect.reload());
});

/**
 * Gulp tasks
 */
gulp.task('build', ['usemin', 'build-assets', 'build-custom']);
gulp.task('default', ['build', 'webserver', 'livereload', 'watch']);
