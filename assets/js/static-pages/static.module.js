(function(){
  'use strict';
  angular
    .module('static', ['ui.router', 'ui.bootstrap'])
    .config(['$stateProvider', function($stateProvider){
      $stateProvider
        .state('static', {
          url: '',
          abstract: true,
          templateUrl: 'templates/static.layout.html'
        })
        .state('static.index', {
          url: '/',
          templateUrl: 'templates/static/index.html'
        });
    }]);
})();
