(function() {
  'use strict';
  angular
    .module('meta')
    .factory('MetaService', [function() {
      var
        title = 'ExekuceInfo',
        listeners = [];

      function triggerUpdate() {
        listeners.forEach(function(listener) {
          listener(title);
        });
      }

      return {
        getTitle: function() {
          return title;
        },
        setTitle: function(newTitle) {
          title = newTitle;
          triggerUpdate();
        },
        registerListener: function(listener) {
          listeners.push(listener);
          return this;
        }
      };
    }]);
})();
