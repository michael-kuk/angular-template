(function(){
  'use strict';
  angular
    .module('meta')
    .controller('MetaController', ['$scope', 'MetaService', function($scope, MetaService){
      $scope.title = MetaService.getTitle();

      function onTitleUpdate(title){
        $scope.title = title;
      }

      MetaService.registerListener(onTitleUpdate);
    }]);
})();
