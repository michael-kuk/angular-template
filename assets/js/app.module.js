(function(){
  'use strict';
  angular
    .module('main', [
      'ui.router',
      'meta',
      'static'
    ])
    .config(['$urlRouterProvider', '$locationProvider', function($urlRouterProvider, $locationProvider){
      $locationProvider.hashPrefix('!');
      $urlRouterProvider.otherwise('/');
    }]);
})();
